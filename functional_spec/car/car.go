package car

import "strings"

type Car struct {
	PlateNumber string
	Color       string
	SlotNumber  string
	Status      string
}

func CreateCar(plateNumber, color string) *Car {
	return &Car{
		PlateNumber: plateNumber,
		Color:       color,
	}
}

// IsEqual is method for check if two object car is identical
func (c *Car) IsEqual(cr Car) bool {
	return (c.PlateNumber == cr.PlateNumber) &&
		(strings.ToLower(c.Color) == strings.ToLower(cr.Color))
}
