package parking

import (
	"reflect"
	"testing"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/car"
	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/slot"
)

func TestCreateParking(t *testing.T) {
	type args struct {
		size uint
	}
	tests := []struct {
		name string
		args args
		want *Parking
	}{
		{
			"Test 1: Create with size=3 produce 3 slots with Index 1 to 3 and null Car",
			args{size: 3},
			&Parking{
				Size: 3,
				Slots: []*slot.Slot{
					{
						Index: 1,
						Car:   nil,
					},
					{
						Index: 2,
						Car:   nil,
					},
					{
						Index: 3,
						Car:   nil,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CreateParking(tt.args.size); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateParking() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParking_FindNearestSlot(t *testing.T) {
	type fields struct {
		Size  uint
		Slots []*slot.Slot
	}
	tests := []struct {
		name    string
		fields  fields
		want    *slot.Slot
		wantErr bool
	}{
		{
			"TestCase 1. Find nearest free slot with nearest filled slot in index 2. Want slot with index=1",
			fields{
				Size: 3,
				Slots: []*slot.Slot{
					{
						Index: 1,
						Car:   nil,
					},
					{
						Index: 2,
						Car:   &car.Car{PlateNumber: "BE4508GE", Color: "Red"},
					},
					{
						Index: 3,
						Car:   nil,
					},
				},
			},
			&slot.Slot{
				Index: 1,
				Car:   nil,
			},
			false,
		},
		{
			"TestCase 2",
			fields{
				Size: 3,
				Slots: []*slot.Slot{
					{
						Index: 1,
						Car:   &car.Car{PlateNumber: "BE4508GE", Color: "Red"},
					},
					{
						Index: 2,
						Car:   nil,
					},
					{
						Index: 3,
						Car:   nil,
					},
				},
			},
			&slot.Slot{
				Index: 2,
				Car:   nil,
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Parking{
				Size:  tt.fields.Size,
				Slots: tt.fields.Slots,
			}
			got, err := p.FindNearestSlot()
			if (err != nil) != tt.wantErr {
				t.Errorf("Parking.FindNearestSlot() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parking.FindNearestSlot() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParking_AddCar(t *testing.T) {
	type fields struct {
		Size  uint
		Slots []*slot.Slot
	}
	type args struct {
		c car.Car
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *slot.Slot
		wantErr bool
	}{
		{
			"Test 1: Parking slots is not full",
			fields{
				Size: 3,
				Slots: []*slot.Slot{
					{
						Index: 1,
						Car:   nil,
					},
					{
						Index: 2,
						Car:   nil,
					},
					{
						Index: 3,
						Car:   nil,
					},
				},
			},
			args{c: car.Car{PlateNumber: "BE4508GE", Color: "Red"}},
			&slot.Slot{
				Index: 1,
				Car:   &car.Car{PlateNumber: "BE4508GE", Color: "Red"},
			},
			false,
		},
		{
			"Test 2: Parking slots is full",
			fields{
				Size: 3,
				Slots: []*slot.Slot{
					{
						Index: 1,
						Car:   &car.Car{PlateNumber: "BE1000GE", Color: "Red"},
					},
					{
						Index: 2,
						Car:   &car.Car{PlateNumber: "BE2000GE", Color: "Red"},
					},
					{
						Index: 3,
						Car:   &car.Car{PlateNumber: "BE3000GE", Color: "Red"},
					},
				},
			},
			args{c: car.Car{PlateNumber: "BE4508GE", Color: "Red"}},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Parking{
				Size:  tt.fields.Size,
				Slots: tt.fields.Slots,
			}
			got, err := p.AddCar(tt.args.c)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parking.AddCar() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parking.AddCar() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParking_GetFilledSlots(t *testing.T) {
	type fields struct {
		Size  uint
		Slots []*slot.Slot
	}
	tests := []struct {
		name            string
		fields          fields
		wantFilledSlots []*slot.Slot
	}{
		{
			"Test 1: Get filled slot",
			fields{
				Size: 3,
				Slots: []*slot.Slot{
					{
						Index: 1,
						Car:   &car.Car{PlateNumber: "BE1000GE", Color: "Red"},
					},
					{
						Index: 2,
						Car:   nil,
					},
					{
						Index: 3,
						Car:   &car.Car{PlateNumber: "BE3000GE", Color: "Red"},
					},
				},
			},
			[]*slot.Slot{
				{
					Index: 1,
					Car:   &car.Car{PlateNumber: "BE1000GE", Color: "Red"},
				},
				{
					Index: 3,
					Car:   &car.Car{PlateNumber: "BE3000GE", Color: "Red"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Parking{
				Size:  tt.fields.Size,
				Slots: tt.fields.Slots,
			}
			if gotFilledSlots := this.GetFilledSlots(); !reflect.DeepEqual(gotFilledSlots, tt.wantFilledSlots) {
				t.Errorf("Parking.GetFilledSlots() = %v, want %v", gotFilledSlots, tt.wantFilledSlots)
			}
		})
	}
}

func TestParking_GetSlotsByCarColor(t *testing.T) {
	type fields struct {
		Size  uint
		Slots []*slot.Slot
	}
	type args struct {
		carColor string
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		wantSlots []*slot.Slot
	}{
		{
			"Test 1: Get Slots by color",
			fields{
				Size: 3,
				Slots: []*slot.Slot{
					{
						Index: 1,
						Car:   &car.Car{PlateNumber: "BE1000GE", Color: "Red"},
					},
					{
						Index: 2,
						Car:   &car.Car{PlateNumber: "BE2000GE", Color: "Blue"},
					},
					{
						Index: 3,
						Car:   &car.Car{PlateNumber: "BE3000GE", Color: "red"},
					},
				},
			},
			args{carColor: "red"},
			[]*slot.Slot{
				{
					Index: 1,
					Car:   &car.Car{PlateNumber: "BE1000GE", Color: "Red"},
				},
				{
					Index: 3,
					Car:   &car.Car{PlateNumber: "BE3000GE", Color: "red"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Parking{
				Size:  tt.fields.Size,
				Slots: tt.fields.Slots,
			}
			if gotSlots := this.GetSlotsByCarColor(tt.args.carColor); !reflect.DeepEqual(gotSlots, tt.wantSlots) {
				t.Errorf("Parking.GetSlotsByCarColor() = %v, want %v", gotSlots, tt.wantSlots)
			}
		})
	}
}

func TestParking_GetSlotByCarPlate(t *testing.T) {
	type fields struct {
		Size  uint
		Slots []*slot.Slot
	}
	type args struct {
		carPlate string
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		wantSlots *slot.Slot
	}{
		{
			"TestCase 1:",
			fields{
				Size: 3,
				Slots: []*slot.Slot{
					{
						Index: 1,
						Car:   &car.Car{PlateNumber: "BE1000GE", Color: "Red"},
					},
					{
						Index: 2,
						Car:   &car.Car{PlateNumber: "BE2000GE", Color: "Blue"},
					},
					{
						Index: 3,
						Car:   &car.Car{PlateNumber: "BE3000GE", Color: "red"},
					},
				},
			},
			args{carPlate: "BE2000GE"},
			&slot.Slot{
				Index: 2,
				Car:   &car.Car{PlateNumber: "BE2000GE", Color: "Blue"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Parking{
				Size:  tt.fields.Size,
				Slots: tt.fields.Slots,
			}
			if gotSlots := this.GetSlotByCarPlate(tt.args.carPlate); !reflect.DeepEqual(gotSlots, tt.wantSlots) {
				t.Errorf("Parking.GetSlotByCarNumber() = %v, want %v", gotSlots, tt.wantSlots)
			}
		})
	}
}

func TestParking_RemoveCar(t *testing.T) {
	type fields struct {
		Size  uint
		Slots []*slot.Slot
	}
	type args struct {
		cr car.Car
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Parking{
				Size:  tt.fields.Size,
				Slots: tt.fields.Slots,
			}
			this.RemoveCar(tt.args.cr)
		})
	}
}

func TestParking_RemoveCarBySlot(t *testing.T) {
	type fields struct {
		Size  uint
		Slots []*slot.Slot
	}
	type args struct {
		slotNumber uint
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Parking{
				Size:  tt.fields.Size,
				Slots: tt.fields.Slots,
			}
			if err := this.RemoveCarBySlot(tt.args.slotNumber); (err != nil) != tt.wantErr {
				t.Errorf("Parking.RemoveCarBySlot() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
