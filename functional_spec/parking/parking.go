package parking

import (
	"fmt"
	"strings"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/car"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/slot"
)

type Parking struct {
	Size  uint
	Slots []*slot.Slot
}

var (
	saved *Parking
	index = 1
)

func CreateParking(size uint) *Parking {
	parking := new(Parking)
	parking.Size = size
	parking.Slots = make([]*slot.Slot, size)
	idx := index
	for i := range parking.Slots {
		parking.Slots[i] = &slot.Slot{Index: uint(idx)}
		idx++
	}
	parking.Save()
	return parking
}

func (p *Parking) Save() {
	saved = p
}

func GetParking() *Parking {
	return saved

}

func (p *Parking) FindNearestSlot() (*slot.Slot, error) {
	for _, s := range p.Slots {
		if s.IsFree() {
			return s, nil
		}
	}
	return nil, fmt.Errorf("sorry, parking slot is full")
}

func (p *Parking) AddCar(c car.Car) (*slot.Slot, error) {
	s, err := p.FindNearestSlot()
	if err != nil {
		return nil, err
	}

	if err := s.Allocate(c); err != nil {
		return nil, err
	}
	return s, nil
}

func (p *Parking) GetFilledSlots() (filledSlots []*slot.Slot) {
	for _, s := range p.Slots {
		if !s.IsFree() {
			filledSlots = append(filledSlots, s)
		}
	}
	return
}

func (p *Parking) GetSlotsByCarColor(carColor string) (slots []*slot.Slot) {
	for _, s := range p.Slots {
		if !s.IsFree() {
			if strings.ToLower(s.GetCarColor()) == strings.ToLower(carColor) {
				slots = append(slots, s)
			}
		}
	}
	return
}

func (p *Parking) GetSlotByCarPlate(carPlate string) (slots *slot.Slot) {
	for _, s := range p.Slots {
		if !s.IsFree() {
			if s.GetCarPlate() == carPlate {
				slots = s
				return
			}
		}
	}
	return
}

func (p *Parking) RemoveCar(c car.Car) {
	for i, s := range p.Slots {
		if !s.IsFree() && s.Car.IsEqual(c) {
			p.Slots[i].Leave()
		}
	}
}

func (p *Parking) RemoveCarBySlot(slotNumber uint) error {
	for i, s := range p.Slots {
		if s.Index == slotNumber {
			p.Slots[i].Car = nil
			return nil
		}
	}
	return fmt.Errorf("slot %d not found", slotNumber)
}
