package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/cmd"
)

const (
	prefix = ">> "
)

func main() {
	fmt.Print(prefix)
	cmd := cmd.NewCommand()
	if len(os.Args) > 1 && os.Args[1] != "" {
		cmdFile, err := os.Open(os.Args[1])
		if err != nil {
			log.Fatal(err)
		}
		defer cmdFile.Close()
		cmdScanner := bufio.NewScanner(cmdFile)
		for cmdScanner.Scan() {
			cmdInput := cmdScanner.Text()
			cmdInput = strings.TrimRight(cmdInput, "\n")
			if cmdInput != "" {
				output := cmd.Run(cmdInput)
				fmt.Println(output)
			}

		}
		os.Exit(0)

		if err := cmdScanner.Err(); err != nil {
			log.Fatal(err)
		}
	}

	reader := bufio.NewReader(os.Stdin)
	for {
		cmdInput, _ := reader.ReadString('\n')
		cmdInput = strings.TrimRight(cmdInput, "\n")
		if cmdInput != "" {
			output := cmd.Run(cmdInput)
			fmt.Println(output)
		}

		fmt.Print(prefix)
	}
}
