package cmd

import "testing"

func TestCommand_Run(t *testing.T) {
	type fields struct {
		Args []string
		Menu map[string]MenuCommand
	}
	type args struct {
		command string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			"Test 1: Test create parking lot with capacity 6 (create_parking_lot 6)",
			fields{
				Menu: map[string]MenuCommand{"create_parking_lot": &CreateParkingLot{}},
			},
			args{command: "create_parking_lot 6"},
			"created a parking lot with 6 slots",
		},
		{
			"Test  2: Test undefined command (e.g zsh)",
			fields{
				Menu: map[string]MenuCommand{"zsh": nil},
			},
			args{command: "zsh"},
			"zsh: command not found",
		},
		{
			"Test 3: Park car to parking slot (park KH9333-200 White)",
			fields{
				Menu: map[string]MenuCommand{"park": &Park{}},
			},
			args{command: "park KA-01-HH-1234 White"},
			"Allocated slot number: 1",
		},
		// {
		// 	"Test 4: Show parking lot status",
		// 	fields{
		// 		Menu: map[string]MenuCommand{"status": &Status{}},
		// 	},
		// 	args{command: "status"},
		// 	`Slot No.    Registration No     Colour
		// 	 1           KA-01-HH-1234       White`,
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Command{
				Args: tt.fields.Args,
				Menu: tt.fields.Menu,
			}
			if got := this.Run(tt.args.command); got != tt.want {
				t.Errorf("Command.Run() = %v, want %v", got, tt.want)
			}
		})
	}
}
