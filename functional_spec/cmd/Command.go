package cmd

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/parking"
)

type MenuCommand interface {
	ParseArgs(string) error
	Clear()
	ValidateParams() bool
	Run() (string, error)
}

type Command struct {
	Args []string
	Menu map[string]MenuCommand
}

var (
	errInvalidParam         = fmt.Errorf("command: invalid parameter(s), please give valid input")
	errParkingNotInitialize = fmt.Errorf("parking: please exec create_parking_lot first")
)

func (cmd *Command) Run(command string) string {
	cmds := strings.SplitN(command, " ", 2)
	menu := cmds[0]
	subCmd, ok := cmd.Menu[menu]
	if command == "exit" {
		os.Exit(0)
	}
	if subCmd == nil || !ok {
		return fmt.Sprintf("%s: command not found", menu)
	}

	// clearing command argument(s)
	subCmd.Clear()

	// get command argument(s)
	if len(cmds) > 1 {
		subCmd.ParseArgs(cmds[1])
	}

	valid := subCmd.ValidateParams()
	if !valid {
		return fmt.Sprintf("%s: invalid parameter(s), please provide valid input", menu)
	}

	park := parking.GetParking()
	if park == nil && menu != "create_parking_lot" {
		return fmt.Sprintf("%v", errParkingNotInitialize)
	}

	output, err := subCmd.Run()
	if err != nil {
		return fmt.Sprintf("%v", err)
	}
	return output
}

// NewCommand for register all command string for control this application
func NewCommand() *Command {
	cmd := new(Command)
	cmd.Menu = make(map[string]MenuCommand)
	cmd.Menu["create_parking_lot"] = new(CreateParkingLot)
	cmd.Menu["park"] = new(Park)
	cmd.Menu["status"] = new(Status)
	cmd.Menu["leave"] = new(Leave)
	cmd.Menu["registration_numbers_for_cars_with_colour"] = new(RegNumberColor)
	cmd.Menu["slot_numbers_for_cars_with_colour"] = new(SlotNumCarColor)
	cmd.Menu["slot_number_for_registration_number"] = new(SlotNumCarPlate)
	return cmd
}
