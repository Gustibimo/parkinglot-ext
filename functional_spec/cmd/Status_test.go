package cmd

import (
	"testing"
)

func TestStatus_ValidateParams(t *testing.T) {
	type fields struct {
		Command Command
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Test 1: valid command with no argument",
			fields{Command: Command{Args: nil}},
			true,
		},

		{
			"Test 1: invalid command caused by inputed argument",
			fields{Command: Command{Args: []string{"park"}}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Status{
				Command: tt.fields.Command,
			}
			if got := this.ValidateParams(); got != tt.want {
				t.Errorf("Status.ValidateParams() = %v, want %v", got, tt.want)
			}
		})
	}
}
