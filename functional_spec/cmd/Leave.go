package cmd

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/parking"
)

type Leave struct {
	Command
	SlotNumber uint
}

func (cmd *Leave) ParseArgs(args string) error {
	cmd.Args = strings.Split(args, " ")
	if !cmd.ValidateParams() {
		return errInvalidParam
	}
	value, err := strconv.ParseUint(args, 0, 64)
	if err != nil {
		return errInvalidParam
	}
	cmd.SlotNumber = uint(value)
	return nil
}

func (cmd *Leave) Clear() {
	cmd.Args = nil
	cmd.SlotNumber = 0
}

func (cmd *Leave) ValidateParams() bool {
	return len(cmd.Args) == 1
}

func (cmd *Leave) Run() (string, error) {
	var output string
	park := parking.GetParking()
	slotNumber := cmd.SlotNumber
	if err := park.RemoveCarBySlot(slotNumber); err != nil {
		return output, err
	}
	output = fmt.Sprintf("Slot number %d is free", slotNumber)
	return output, nil
}
