package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/parking"
)

type Status struct {
	Command
}

func (cmd *Status) ParseArgs(args string) error {
	return nil
}

func (cmd *Status) Clear() {
	cmd.Args = nil
}

func (cmd *Status) ValidateParams() bool {
	return len(cmd.Args) == 0
}

func (cmd *Status) Run() (string, error) {
	var list = []string{fmt.Sprintf("%-12s%-20s%-10s", "Slot No.", "Registration No", "Colour")}
	filledSlots := parking.GetParking().GetFilledSlots()
	for _, s := range filledSlots {
		c := s.Car
		list = append(list, fmt.Sprintf("%-12v%-20v%-10v", s.Index, c.PlateNumber, c.Color))
	}
	output := strings.Join(list, "\n")
	return output, nil
}
