package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/parking"
)

type SlotNumCarPlate struct {
	Command
	CarPlate string
}

func (cmd *SlotNumCarPlate) ParseArgs(args string) error {
	cmd.Args = strings.Split(args, " ")
	if !cmd.ValidateParams() {
		return errInvalidParam
	}
	cmd.CarPlate = cmd.Args[0]
	return nil
}

func (cmd *SlotNumCarPlate) Clear() {
	cmd.Args = nil
	cmd.CarPlate = ""
}

func (cmd *SlotNumCarPlate) ValidateParams() bool {
	return len(cmd.Args) == 1 && cmd.Args[0] != ""
}

func (cmd *SlotNumCarPlate) Run() (string, error) {
	var output string
	slot := parking.GetParking().GetSlotByCarPlate(cmd.CarPlate)
	if slot == nil {
		return "Not found", nil
	}
	output = fmt.Sprint(slot.Index)
	return output, nil
}
