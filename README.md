# gojek-parkinglot

## Project Requirement

- Go 1.13.6 https://golang.org/doc/install
- Docker

## Build

<pre>
$ bin/setup
</pre>

# Run interactive CLI
```
$ bin/parking_lot
>>>
```

## Usage

Create parking lot:
```
>>> create_parking_lot [size]
>>> Created a parking lot with [size] slots
```

Park a Car:

```
>>> park [plateNumber] [car color]
>>> Allocated slot number: 1
```

Free a slot:

```
>>> leave [slot number]
>>> Slot number [number] is free
```

Show parking lot status:

```
>>> status
>>> Slot No.    Registration No.    Colour
    1           [plate number]      [car color]
    2           [plate number]      [car color]
    3           [plate number]      [car color]
    ...         ...                 ...
```

Retrieve plate number by color:

```
>>> registration_numbers_for_cars_with_colour [car color]
>>> [plate number 1], [plate number 2], [plate number 3], ...
```

Retrieve slot number of car using car color:

```
>>> slot_numbers_for_cars_with_colour [car color]
>>> [slot number 1], [slot number 2], ...
```

Retrieve slot number of car using plate number:

```
>>> slot_number_for_registration_number [plate number]
>>> [slot number]
```

Exit program:

```
>>> exit
```


# Problem Statement:

I own a parking lot that can hold up to 'n' cars at any given point in time. Each slot is given a number starting at 1 increasing with increasing distance from the entry point
in steps of one. I want to create an automated ticketing system that allows my customers to use my parking lot without human intervention.

The system has a following features:

- Add car to certain slot in the parking lot by car plate number and car colour.
- Retrieve car plate number of a particular colour.
- Slot number in which a car with a given registration number is parked.
- Slot numbers of all slots where a car of a particular colour is parked

# Specs:

<pre>

(entity) Car
- plateNumber
- color

(entity) Slot
- Index
- Car

(entity) Parking
- size
- slots


(service)

Park:
- Add new car to parking slot
- Add new slot to Slot
- Remove car from selected slot

GetSlot:
- Retrieve slot number by car color
- Retrieve slot number by car plate number
- Retrieve car plate number for selected car color

</pre>

# Testing

## unit test
 
 
 - **parking**
 ```
$ cd functional_spec/parking
$ go test -v
 ```
 - **car**
  ```
$ cd functional_spec/car
$ go test -v
 ```
 
 - **slot**
  ```
$ cd functional_spec/slot
$ go test -v
 ```
 
 - **cmd**
  ```
$ cd functional_spec/cmd
$ go test -v
 ```
 
